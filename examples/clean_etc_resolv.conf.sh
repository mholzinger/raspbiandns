#!/bin/bash

# Place file in /usr/local/bin/clean_etc_resolv.conf.sh
# cronjob
# sudo crontab -e
# The purpose of this script is to wipe out any creeping IBM DNS entries from /etc/resolv.conf
# * * * * * sudo /usr/local/bin/clean_etc_resolv.conf.sh

# The purpose of this script is to wipe out any creeping IBM DNS entries from /etc/resolv.conf
# Adding this to cron once a minute should be sufficient

sudo sed -i  /etc/resolv.conf -e '/domain\ uswa.ibm.com/d' -e '/9.0.1/d' -e 's/\ uswa.ibm.com//g'
